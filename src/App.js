import { Component } from 'react'
import Dashboard from './pages/Dashboard.js'
import Login from './pages/Login.js'
import Signup from './pages/Signup.js'
import User from './pages/User.js'
import Header from './components/layout/Header'
import Base from './utils/Base'
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  useLocation
} from "react-router-dom"
import './App.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      auth: false
    }
  }

  updateAuth(){
    console.log("updateAuth");
    console.log(this.props.authenticated);
    if (this.props.authenticated) {
      console.log("dash");
      return(
        <Redirect to="/dashboard" />
      );
    }
    else {
      console.log("login");
      return <Redirect to="/login" />
    }
  }

  componentDidUpdate(prevProps) {
    if ( prevProps.authenticated !== this.props.authenticated ) {
      this.updateAuth(); //example calling redux action
    }
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.authenticated) {
      return {
        auth: nextProps.authenticated,
      };
    } else {
      return null;
    }
  }

  render() {
 
    return (

      <div className="App">
        <Router>
          <Header />
          <Base className="Marig-top">
            <Switch>
              <Redirect exact from="/" to="/dashboard" />
              <PrivateRoute path="/dashboard" auth={this.state.auth}>
                <Dashboard />
              </PrivateRoute>
              <PrivateRoute path="/user" auth={this.state.auth}>
                <User />
              </PrivateRoute>
              <Route path="/login" component={Login} />
              <Route path="/signup" component={Signup} />
              <Route path="*">
                <NoMatch />
              </Route>
            </Switch>
          </Base>
        </Router>
      </div>
    );
  }
}

function PrivateRoute({ children, auth, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h1>ERROR 404</h1>
      <h3>
        This path doesn't exist <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

App.propTypes = {
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps)(App);