import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  top : {
    marginTop : 80
  }
}))

export default function Base(props) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container fixed className={classes.top}>
        {props.children}
      </Container>
    </React.Fragment>
  );
}