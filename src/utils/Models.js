const API = {
  signup: {
    token:
      "eyJhbGciOiJSUzI1NiIsImtpZCI6IjFiYjk2MDVjMzZlOThlMzAxMTdhNjk1MTc1NjkzODY4MzAyMDJiMmQiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20veHBpa2FuZHJvaWQiLCJhdWQiOiJ4cGlrYW5kcm9pZCIsImF1dGhfdGltZSI6MTYyNzUwNDM1NiwidXNlcl9pZCI6IkhpclBxelVINWRReDBOM2k4UU1mck9LNWVrUjIiLCJzdWIiOiJIaXJQcXpVSDVkUXgwTjNpOFFNZnJPSzVla1IyIiwiaWF0IjoxNjI3NTA0MzU2LCJleHAiOjE2Mjc1MDc5NTYsImVtYWlsIjoiYWRiZXJhcmRpQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJhZGJlcmFyZGlAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.oe94-pXRQNQB7y6UoOabI9DQBr7tBJh-8JnFkhDYCZwiq1IlJZDpEgW3KFWJPnt_CJO62edowpBFzph6ZbeJndsbFxLy7y53Qgs440a_XsB2akxw2DLyX9ul3u9N6MyzAQnjgZjW1O_LUwB3UKpmOw9LsPsbIyiQSt9P6naiEqaiujlLZ9N6CjenX8DmFvwKI8IHPUtV2Wip53Ji5_h8HPUG6avtJ-jp-oOSWVv12MH84fALJL2Vc-jIhXSnLSzGbd_ZZCR5uVGnIpV9XRqYaRv4uvVhuhJXhNBdzTHEbriyWEi0rA9AXu4lHlz2Hx6_E8W5kHiu5ktp9XEtDFxmTg",
  },

  // ----------------------------------- getAllXpik ------------------------------------------------ //

  getAllXpik: [
    /* Array que contiene la lista de xpik que no son privados, la respuesta se hace a /api/xpiks
        getAllXpiks, request all public xpiks, Solo el array es la respuesta, sin incluir el nombre de la propiedad*/
    {
      title: "Perro caliente sin cebolla",
      xpikId: "S5vqZer18k9yzQxgVRPn",
      createdAt: 1623876509869,
      userHandle: "otroantonio12",
      thumbnail: "path://unlugar/unafoto.jpg",
      content: {
        tomate: 0,
        "Perro caliente": 0,
        queso: 0,
        "sin cebolla": 0,
      },
    },
  ],

  // ----------------------------------- getXpik ------------------------------------------------ //

  getXpik: {
    // Respuesta de getXpik a /api/xpik/:xpik/ La respuesta no incluye el nombre de la propiedad
    title: "Perro caliente sin cebolla",
    xpikId: "T6MEaBuY5B6djcVbXGfT",
    createdAt: 1627410163830,
    userHandle: "otroantonio",
    thumbnail: "path://unlugar/unafoto.jpg",
    visibility: "draft",
    content: {
      0: "Perro caliente",
      1: "queso",
      2: "tomate",
      3: "sin cebolla",
    },
  },

  // ----------------------------------- getXpikPublic ------------------------------------------------ //

  // getXpikPublic Se usa solo para Android no se incluirá aquí

  // ----------------------------------- createXpik ------------------------------------------------ //

  /* Body de post create xpik {
    "title" : "Quiero cruzar la calle con mi gallina",
    "thumbnail" : "path://unlugar/unafoto.jpg",
    "content" : [
        "gallina", "calle", "persona"]
    } */

    createXpik:
    {// Respuesta de crear un nuevo xpik por medio de /api/xpik, el id de abajo es el que se le asigno en firebase, no incluye el nombre de la propiedad
        id: "LgCbAriwGQn7q2NK1afP"
    },

    // ----------------------------------- deleteXpik ------------------------------------------------ // 

    // Se borra el xpik con el id que se envía por: /api/xpik/:xpikId/

    // ----------------------------------- updateXpik ------------------------------------------------ //

    // Actualiza el/los datos del body al xpik con el id que se envía por: /api/xpik/:xpik/

    // ----------------------------------- getAllWords ------------------------------------------------ // 

    // Obtiene todas las palabras de emojis enviando: /api/words, la respuesta no incluye el nombre de la propiedad

    words: {
        "0bicycle": [
          "ic_emoji_0bicycle"
        ],
        "0eighteen": [
          "ic_emoji_under_0eighteen"
        ],
        "0potable": [
          "ic_emoji_water_0potable"
        ],
        "0smartphone": [
          "ic_emoji_0smartphone"
        ],
        "0smoke": [
          "ic_emoji_0smoke"
        ],
        "0walk": [
          "ic_emoji_0walk"
        ],
        "add": [
          "ic_emoji_symbol_add"
        ],
        "airplane": [
          "ic_emoji_airplane"
        ],
        "ambulance": [
          "ic_emoji_ambulance"
        ],
        "angry": [
          "ic_emoji_angry"
        ],
        "ant": [
          "ic_emoji_ant"
        ],
        "apple": [
          "ic_emoji_apple"
        ],
        "arepa": [
          "ic_emoji_arepa"
        ],
        "arm": [],
        "arrival": [
          "ic_emoji_arrival"
        ],
        "asterisk": [
          "ic_emoji_asterisk"
        ],
        "astronaut": [
          "ic_emoji_astronaut",
          "ic_emoji_woman_astronaut"
        ],
        "atm": [
          "ic_emoji_atm"
        ],
        "avocado": [
          "ic_emoji_avocado"
        ],
        "axe": [
          "ic_emoji_axe"
        ],
        "baby": [
          "ic_emoji_baby",
          "ic_emoji_sign_baby"
        ],
        "baby1bottle": [
          "ic_emoji_bottle_baby"
        ],
        "bacon": [
          "ic_emoji_bacon"
        ],
        "badger": [
          "ic_emoji_badger"
        ],
        "bag": [
          "ic_emoji_hand_bag"
        ],
        "bagel": [
          "ic_emoji_bagel"
        ],
        "baggage": [
          "ic_emoji_baggage"
        ],
        "ballet": [
          "ic_emoji_shoes_ballet"
        ],
        "banana": [
          "ic_emoji_banana"
        ],
        "band1aid": [
          "ic_emoji_band1aid"
        ],
        "bank": [
          "ic_emoji_bank"
        ],
        "barber": [
          "ic_emoji_pole_barber"
        ],
        "basket": [
          "ic_emoji_basket"
        ],
        "bat": [
          "ic_emoji_bat"
        ],
        "bathe": [
          "ic_emoji_bathe"
        ],
        "bathroom": [
          "ic_emoji_sign_bathroom",
          "ic_emoji_toilet",
          "ic_emoji_paper_toilet"
        ],
        "bathtub": [
          "ic_emoji_bathtub"
        ],
        "beach": [
          "ic_emoji_beach"
        ],
        "bear": [
          "ic_emoji_bear",
          "ic_emoji_polar_bear"
        ],
        "beaver": [
          "ic_emoji_beaver"
        ],
        "bed": [
          "ic_emoji_bed"
        ],
        "bee": [
          "ic_emoji_bee"
        ],
        "beer": [
          "ic_emoji_beer"
        ],
        "bicycle": [
          "ic_emoji_bicycle",
          "ic_emoji_0bicycle"
        ],
        "bighorn": [
          "ic_emoji_ram_bighorn"
        ],
        "bikini": [
          "ic_emoji_bikini"
        ],
        "birthday": [
          "ic_emoji_birthday"
        ],
        "bison": [
          "ic_emoji_bison"
        ],
        "blind": [
          "ic_emoji_man_blind",
          "ic_emoji_woman_blind"
        ],
        "blood": [
          "ic_emoji_a_blood",
          "ic_emoji_b_blood",
          "ic_emoji_ab_blood",
          "ic_emoji_o_blood"
        ],
        "blouse": [
          "ic_emoji_blouse"
        ],
        "blowfish": [
          "ic_emoji_blowfish"
        ],
        "boat": [
          "ic_emoji_boat"
        ],
        "boot": [
          "ic_emoji_boot"
        ],
        "boring": [
          "ic_emoji_boring"
        ],
        "bowling": [
          "ic_emoji_bowling"
        ],
        "boxing": [
          "ic_emoji_boxing"
        ],
        "boy": [
          "ic_emoji_boy"
        ],
        "bread": [
          "ic_emoji_white_bread",
          "ic_emoji_baguette_bread"
        ],
        "broccoli": [
          "ic_emoji_broccoli"
        ],
        "broom": [
          "ic_emoji_broom"
        ],
        "brother": [
          "ic_emoji_man_single_brother"
        ],
        "bulb": [
          "ic_emoji_bulb"
        ],
        "bull": [
          "ic_emoji_bull"
        ],
        "burguer": [
          "ic_emoji_burguer"
        ],
        "bus": [
          "ic_emoji_bus",
          "ic_emoji_stop_bus"
        ],
        "butter": [
          "ic_emoji_butter"
        ],
        "butterfly": [
          "ic_emoji_butterfly"
        ],
        "cake": [
          "ic_emoji_cake"
        ],
        "camel": [
          "ic_emoji_camel"
        ],
        "camping": [
          "ic_emoji_camping"
        ],
        "can": [
          "ic_emoji_can"
        ],
        "candle": [
          "ic_emoji_candle"
        ],
        "cap": [
          "ic_emoji_cap"
        ],
        "car": [
          "ic_emoji_car"
        ],
        "carousel": [
          "ic_emoji_carousel"
        ],
        "carrot": [
          "ic_emoji_carrot"
        ],
        "casino": [
          "ic_emoji_casino"
        ],
        "castle": [
          "ic_emoji_castle"
        ],
        "cat": [
          "ic_emoji_cat"
        ],
        "caterpillar": [
          "ic_emoji_caterpillar"
        ],
        "celebrate": [
          "ic_emoji_celebrate"
        ],
        "chair": [
          "ic_emoji_chair"
        ],
        "chard": [
          "ic_emoji_chard"
        ],
        "cheese": [
          "ic_emoji_cheese"
        ],
        "chef": [
          "ic_emoji_man_chef",
          "ic_emoji_woman_chef"
        ],
        "chess": [
          "ic_emoji_chess"
        ],
        "chicken": [
          "ic_emoji_chicken"
        ],
        "chili": [
          "ic_emoji_chili"
        ],
        "chocolate": [
          "ic_emoji_chocolate"
        ],
        "chopsticks": [
          "ic_emoji_chopsticks"
        ],
        "christmas": [
          "ic_emoji_tree_christmas"
        ],
        "church": [
          "ic_emoji_church"
        ],
        "cigar": [
          "ic_emoji_cigar"
        ],
        "circus": [
          "ic_emoji_circus"
        ],
        "city": [
          "ic_emoji_city"
        ],
        "clam": [
          "ic_emoji_clam"
        ],
        "climbing": [
          "ic_emoji_man_climbing",
          "ic_emoji_woman_climbing"
        ],
        "clip": [
          "ic_emoji_clip"
        ],
        "clown": [
          "ic_emoji_clown"
        ],
        "cockroach": [
          "ic_emoji_cockroach"
        ],
        "coconut": [
          "ic_emoji_coconut"
        ],
        "coffee": [
          "ic_emoji_coffee"
        ],
        "compass": [
          "ic_emoji_compass"
        ],
        "cookie": [
          "ic_emoji_cookie"
        ],
        "cool": [
          "ic_emoji_cool"
        ],
        "corn": [
          "ic_emoji_corn"
        ],
        "couple": [
          "ic_emoji_couple",
          "ic_emoji_lesbian_couple",
          "ic_emoji_gay_couple"
        ],
        "cow": [
          "ic_emoji_cow"
        ],
        "crab": [
          "ic_emoji_crab"
        ],
        "credit1card": [
          "ic_emoji_credit_card"
        ],
        "cripple": [
          "ic_emoji_man_cripple",
          "ic_emoji_woman_cripple"
        ]
      },

    // ----------------------------------- getAllWordsByLanguage ------------------------------------------------ // 

    // Obtiene todas las palabras de emojis enviando: api/words/l/:language, la respuesta no incluye el nombre de la propiedad

    wordsEn: {
      add: "add",
      "airplane": "airplane",
      "ambulance": "ambulance",
      "angry": "angry",
      "ant": "ant",
      "apple": "apple",
      "arepa": "arepa",
      "arm": "arm",
      "arrival": "arrival",
      "asterisk": "asterisk",
      "astronaut": "astronaut",
      "atm": "atm",
      "avocado": "avocado",
      "axe": "axe",
      "baby": "baby",
      "baby bottle": "baby1bottle",
      "bacon": "bacon",
      "badger": "badger",
      "bag": "bag",
      "bagel": "bagel",
      "baggage": "baggage",
      "ballet": "ballet",
      "banana": "banana",
      "band aid": "band1aid",
      "bank": "bank",
      "barber": "barber",
      "basket": "basket",
      "bat": "bat",
      "bathe": "bathe",
      "bathroom": "bathroom",
      "bathtub": "bathtub",
      "beach": "beach",
      "bear": "bear",
      "beaver": "beaver",
      "bed": "bed",
      "bee": "bee",
      "beer": "beer",
      "bicycle": "bicycle",
      "bighorn": "bighorn",
      "bikini": "bikini",
      "birthday": "birthday",
      "bison": "bison",
      "blind": "blind",
      "blood": "blood",
      "blouse": "blouse",
      "blowfish": "blowfish",
      "boat": "boat",
      "boot": "boot",
      "boring": "boring",
      "bowling": "bowling",
      "boxing": "boxing",
      "boy": "boy",
      "bread": "bread",
      "broccoli": "broccoli",
      "broom": "broom",
      "brother": "brother",
      "bulb": "bulb",
      "bull": "bull",
      "burguer": "burguer",
      "bus": "bus",
      "butter": "butter",
      "butterfly": "butterfly",
      "cake": "cake",
      "camel": "camel",
      "camping": "camping",
      "can": "can",
      "candle": "candle",
      "cap": "cap",
      "car": "car",
      "card": "card",
      "carousel": "carousel",
      "carrot": "carrot",
      "casino": "casino",
      "castle": "castle",
      "cat": "cat",
      "caterpillar": "caterpillar",
      "celebrate": "celebrate",
      "chair": "chair",
      "chard": "chard",
      "cheese": "cheese",
      "chef": "chef",
      "chess": "chess",
      "chicken": "chicken",
      "chili": "chili",
      "chocolate": "chocolate",
      "chopsticks": "chopsticks",
      "christmas": "christmas",
      "church": "church",
      "cigar": "cigar",
      "circus": "circus",
      "city": "city",
      "clam": "clam",
      "climbing": "climbing",
      "clip": "clip",
      "clown": "clown",
      "cockroach": "cockroach",
      "coconut": "coconut",
      "coffee": "coffee",
      "compass": "compass",
      "cookie": "cookie",
      "cool": "cool",
      "corn": "corn",
      "couple": "couple",
      "cow": "cow",
      "crab": "crab",
      "cripple": "cripple",
      "crocodile": "crocodile",
      "croissant": "croissant",
      "cucumber": "cucumber",
      "curling": "curling",
      "curry": "curry",
      "customs": "customs",
      "cutlery": "cutlery",
      "cyclist": "cyclist",
      "dance": "dance",
      "daughter": "daughter",
      "deer": "deer",
      "departure": "departure",
      "desert": "desert",
      "detective": "detective",
      "devil": "devil",
      "disgusting": "disgusting",
      "divide": "divide",
      "doctor": "doctor",
      "dog": "dog",
      "dollar": "dollar",
      "dolphin": "dolphin",
      "donut": "donut",
      "dress": "dress",
      "duck": "duck",
      "eagle": "eagle",
      "egg": "egg",
      "eggplant": "eggplant",
      "eggs": "eggs",
      "eight": "eight",
      "elephant": "elephant",
      "elevator": "elevator",
      "euro": "euro",
      "exchange": "exchange",
      "factory": "factory",
      "family": "family",
      "farmer": "farmer",
      "female": "female",
      "ferris wheel": "ferris1wheel",
      "ferry": "ferry",
      "fever": "fever",
      "fire": "fire",
      "firefighter": "firefighter",
      "firetruck": "firetruck",
      "fish": "fish",
      "fishing": "fishing",
      "five": "five",
      "flamingo": "flamingo",
      "flashlight": "flashlight",
      "flip flop": "flip1flop",
      "fondue": "fondue",
      "food": "food",
      "fountain": "fountain",
      "four": "four",
      "fox": "fox",
      "freezing": "freezing",
      "fries": "fries",
      "frog": "frog",
      "fuel": "fuel",
      "game": "game",
      "garlic": "garlic",
      "ghost": "ghost",
      "gift": "gift",
      "giraffe": "giraffe",
      "girl": "girl",
      "gloves": "gloves",
      "goat": "goat",
      "golf": "golf",
      "gorilla": "gorilla",
      "grapes": "grapes",
      "grasshopper": "grasshopper",
      "greet": "greet",
      "guitar": "guitar",
      "haircut": "haircut",
      "halloween": "halloween",
      "hamburguer": "burguer",
      "hammer": "hammer",
      "hamster": "hamster",
      "handsaw": "handsaw",
      "hat": "hat",
      "headphones": "headphones",
      "heart": "heart",
      "heated": "heated",
      "hedgehog": "hedgehog",
      "helicopter": "helicopter",
      "hello": "hello",
      "high heel": "high1heel",
      "hijab": "hijab",
      "hippo": "hippo",
      "honey": "honey",
      "horse": "horse",
      "hospital": "hospital",
      "hot": "hot",
      "hotdog": "hotdog",
      "hotel": "hotel",
      "house": "house",
      "hundred": "hundred",
      "hungry": "hungry",
      "husband": "husband",
      "ice": "ice",
      "icecream": "icecream",
      "information": "information",
      "injection": "injection",
      "insect fly": "insect1fly",
      "islam": "islam",
      "island": "island",
      "jacket": "jacket",
      "jeans": "jeans",
      "judaism": "judaism",
      "judge": "judge",
      "kangaroo": "kangaroo",
      "karate": "karate",
      "key card": "key1card",
      "kimono": "kimono",
      "kiss": "kiss",
      "kiwi": "kiwi",
      "knife": "knife",
      "koala": "koala",
      "lacrosse": "lacrosse",
      "ladybug": "ladybug",
      "laptop": "laptop",
      "latin": "latin",
      "laugh": "laugh",
      "lemon": "lemon",
      "leopard": "leopard",
      "lgbt": "lgbt",
      "liberty": "liberty",
      "lion": "lion",
      "lipstick": "lipstick",
      "litter": "litter",
      "lizard": "lizard",
      "llama": "llama",
      "lobster": "lobster",
      "locker": "locker",
      "love": "love",
      "luck": "luck",
      "male": "male",
      "man": "man",
      "mango": "mango",
      "manicure": "manicure",
      "map": "map",
      "massage": "massage",
      "meat": "meat",
      "mechanic": "mechanic",
      "medical": "medical",
      "medicine": "medicine",
      "meditate": "meditate",
      "menorah": "menorah",
      "mice": "mice",
      "milk": "milk",
      "mirror": "mirror",
      "money": "money",
      "monkey": "monkey",
      "moon": "moon",
      "mosque": "mosque",
      "mosquito": "mosquito",
      "mountain": "mountain",
      "mouse": "mouse",
      "muffin": "muffin",
      "multiply": "multiply",
      "museum": "museum",
      "muted": "muted",
      "newspaper": "newspaper",
      "night": "night",
      "nine": "nine",
      "no bicycle": "0bicycle",
      "no eighteen": "0eighteen",
      "no potable": "0potable",
      "no smartphone": "0smartphone",
      "no smoke": "0smoke",
      "no walk": "0walk",
      "number": "number",
      "octopus": "octopus",
      "ok": "ok",
      "old man": "old1man",
      "old woman": "old1woman",
      "one": "one",
      "onion": "onion",
      "orange": "orange",
      "orangutan": "orangutan",
      "otter": "otter",
      "owl": "owl",
      "paella": "paella",
      "paintbrush": "paintbrush",
      "painter": "painter",
      "painting": "painting",
      "pancake": "pancake",
      "panda": "panda",
      "paper toilet": "paper1toilet",
      "paprika": "paprika",
      "paragliding": "paragliding",
      "park": "park",
      "parking": "parking",
      "parrot": "parrot",
      "passport": "passport",
      "pasta": "pasta",
      "patrol": "patrol",
      "peace": "peace",
      "peach": "peach",
      "peacock": "peacock",
      "pear": "pear",
      "pen": "pen",
      "pencil": "pencil",
      "penguin": "penguin",
      "picture": "picture",
      "pie": "pie",
      "pig": "pig",
      "pilot": "pilot",
      "pineapple": "pineapple",
      "pizza": "pizza",
      "please": "please",
      "plug": "plug",
      "plunger": "plunger",
      "police": "police",
      "poop": "poop",
      "popcorn": "popcorn",
      "post mail": "post1mail",
      "potable": "potable",
      "potato": "potato",
      "pound": "pound",
      "pregnant": "pregnant",
      "pretzel": "pretzel",
      "programmer": "programmer",
      "pudding": "pudding",
      "purse": "purse",
      "puzzle": "puzzle",
      "puzzled": "puzzled",
      "question": "question",
      "rabbit": "rabbit",
      "racoon": "racoon",
      "radioactive": "radioactive",
      "rail": "rail",
      "railway": "railway",
      "rain": "rain",
      "ramen": "ramen",
      "reception": "reception",
      "recycle": "recycle",
      "restroom": "restroom",
      "rhino": "rhino",
      "rice": "rice",
      "ring": "ring",
      "road": "road",
      "rollercoaster": "rollercoaster",
      "rooster": "rooster",
      "rowing": "rowing",
      "sad": "sad",
      "salad": "salad",
      "salt": "salt",
      "sandwich": "sandwich",
      "santa": "santa",
      "sauna": "sauna",
      "scared": "scared",
      "scarf": "scarf",
      "school": "school",
      "scientist": "scientist",
      "scooter": "scooter",
      "scorpion": "scorpion",
      "screwdriver": "screwdriver",
      "scuba dive": "scuba1dive",
      "seal": "seal",
      "seat": "seat",
      "seven": "seven",
      "sewing": "sewing",
      "shark": "shark",
      "shave": "shave",
      "shawarma": "shawarma",
      "sheep": "sheep",
      "sheet": "sheet",
      "ship": "ship",
      "shirt": "shirt",
      "shoe": "shoe",
      "shopping": "shopping",
      "short": "short",
      "shower": "shower",
      "shrimp": "shrimp",
      "sick": "sick",
      "silence": "silence",
      "sing": "sing",
      "six": "six",
      "skates": "skates",
      "ski": "ski",
      "skunk": "skunk",
      "sleep": "sleep",
      "sloth": "sloth",
      "small": "small",
      "smart": "smart",
      "smartphone": "smartphone",
      "smile": "smile",
      "snail": "snail",
      "snake": "snake",
      "snow": "snow",
      "soap": "soap",
      "socks": "socks",
      "son": "son",
      "sos": "sos",
      "spider": "spider",
      "sponge": "sponge",
      "squid": "squid",
      "squirrel": "squirrel",
      "stadium": "stadium",
      "stair": "stair",
      "stars": "stars",
      "station": "station",
      "store": "store",
      "strawberry": "strawberry",
      "strong": "strong",
      "student": "student",
      "substract": "substract",
      "suitcase": "suitcase",
      "sun": "sun",
      "surfer": "surfer",
      "sushi": "sushi",
      "swim": "swim",
      "swimsuit": "swimsuit",
      "synagogue": "synagogue",
      "taco": "taco",
      "tasty": "tasty",
      "taxi": "taxi",
      "tea": "tea",
      "teacher": "teacher",
      "teapot": "teapot",
      "teardrops": "teardrops",
      "ten": "ten",
      "tennis": "tennis",
      "theater": "theater",
      "three": "three",
      "thumb down": "thumb1down",
      "thumb up": "thumb1up",
      "ticket": "ticket",
      "tickets": "tickets",
      "tiger": "tiger",
      "timer": "timer",
      "toast": "toast",
      "toilet": "toilet",
      "tomato": "tomato",
      "toothbrush": "toothbrush",
      "toxic": "toxic",
      "toy": "toy",
      "train": "train",
      "transexual": "transexual",
      "trash": "trash",
      "troubled": "troubled",
      "turkey": "turkey",
      "turtle": "turtle",
      "two": "two",
      "umbrella": "umbrella",
      "uncork": "uncork",
      "underwear": "underwear",
      "upset": "upset",
      "urn": "urn",
      "violin": "violin",
      "volcano": "volcano",
      "vomit": "vomit",
      "waffles": "waffles",
      "walk": "walk",
      "warning": "warning",
      "water": "water",
      "watermelon": "watermelon",
      "wave": "wave",
      "weightlifter": "weightlifter",
      "whale": "whale",
      "wheelchair": "wheelchair",
      "wife": "wife",
      "wine": "wine",
      "wink": "wink",
      "wolf": "wolf",
      "woman": "woman",
      "worm": "worm",
      "worship": "worship",
      "wound": "wound",
      "wrench": "wrench",
      "writing": "writing",
      "yuan": "yuan",
      "zebra": "zebra",
      "zero": "zero"
    },

    // ----------------------------------- getNoKeys ------------------------------------------------ // 

    // Recibe los keys que todavía no tienen alias para el idioma enviado por: api/checker/:language, la respuesta no incluye el nombre de la propiedad

    noAliasFor: {
      language: "es",
      words: [
        "bagel",
        "credit1card",
        "doctor",
        "female",
        "litter",
        "one",
        "restroom"
      ]
    },

    // ----------------------------------- createKey ------------------------------------------------ // 

    /* Body de post create key 
    {
    "files" : [
        "ic_photo_gallinaconpower",
        "ic_photo_rabipelaosalvaje",
        "ic_photo_macarronesasesinos"]
    } */
    // Crea el key indicando el id o nombre por: api/key/:key la respuesta no incluye el nombre de la propiedad
    // :key = nombre de la palabra, ej: perro

    keyName: {
      key: "gallinaconpower"
    },

    // ----------------------------------- updateKey ------------------------------------------------ // 

    /*
    updateBody: {
      "files" : [
          "ic_photo_gallinaconpower",
          "ic_photo_rabipelaosalvaje",
          "ic_photo_mapachespresos"]
    },*/

    // Actualiza los datos del body enviado al key por: api/key/:key la respuesta no incluye el nombre de la propiedad

    keyName: {
      "key": "gallinaconpower"
    },

    // ----------------------------------- deleteKey ------------------------------------------------ // 

    // Borra el key enviando su id (nombre) por: api/key/:key

    deletemsg: {
      "message": "gallinaconpower Key was delete"
    },

    // ----------------------------------- createAlias ------------------------------------------------ // 

    // Añade los alias por idioma pasando el id del key por: /alias/:key

    /* Body de post create Alias
    AliasListPerLanguage: {
      alias : {
      "ar" :"pollitoingles",
      "en" : "pollitoingles",
      "de": "pollitoingles",
      "es": "pollitoingles",
      "fr": "pollitoingles",
      "hi": "pollitoingles",
      "it": "pollitoingles",
      "iw": "pollitoingles",
      "ja": "pollitoingles",
      "ko": "pollitoingles",
      "pt": "pollitoingles",
      "ru": "pollitoingles",
      "tr": "pollitoingles",
      "zh": "pollitoingles"
      }
    }*/

    // Respuesta no incluye el nombre de la propiedad
    msg: {
      message: "success"
    },



    // ----------------------------------- deleteAlias ------------------------------------------------ // 

    // Se borra el alias en el idioma que se indique por: /alias/:language/:alias

    // Respuesta no incluye el nombre de la propiedad
    msg: {
      message: "pollitoingles alias was delete"
    },

    // ----------------------------------- createCategory ------------------------------------------------ // 

    // Se crea una nueva categoría en cms_categories por medio de: api/category

    /* Body de post create Category (Create new category with all titles (All languages))

    categoryList: {
      "titles" : {
      "en" : "asdfg",
      "es": "asdfg",
      "fr": "asdfg",
      "it": "asdfg",
      "pt": "asdfg",
      "ru": "asdfg",
      "zh": "asdfg",
      "zh_TW": "asdfg"
      }
    }*/

    response: {
      "message": "succesfully"
    },

    // ----------------------------------- deleteCategory ------------------------------------------------ // 

    // Se borra la categoría al pasar su id por: /category/:category

    response: {
      "message": "succesfully"
    },

  // ----------------------------------- login ------------------------------------------------ //

  // Respuesta que se consigue por /api/login (Login to XPIK)
  login: {
    token: "eyJhbGciOiJSUzI1NiIsImtpZCI6IjFiYjk2MDVjMzZlOThlMzAxMTdhNjk1MTc1NjkzODY4MzAyMDJiMmQiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20veHBpa2FuZHJvaWQiLCJhdWQiOiJ4cGlrYW5kcm9pZCIsImF1dGhfdGltZSI6MTYyNzQ5NTM1NiwidXNlcl9pZCI6InNCTGFXMkludHRPYjdhQk1IeEc2S0NMVjBMcjIiLCJzdWIiOiJzQkxhVzJJbnR0T2I3YUJNSHhHNktDTFYwTHIyIiwiaWF0IjoxNjI3NDk1MzU2LCJleHAiOjE2Mjc0OTg5NTYsImVtYWlsIjoiYW50b25pb0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiYW50b25pb0BnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.o1AwK6RO3RcFicz_6h1zyuMp6pHNHvQ6XPxkC7WgY_qMv_N9AVPzKO8w-RL9MUenMoQ2wlIJfkOfWacJ0WABlsWb4c7HtS__t-hn6kMGaqj93hqplkUjNQn40isGvF4tlamWpHC06q3m8XkXgMPJMojvZ0k37bSTrhNdd0Fv1tIUogLoDd6-y-ACuZ4WEjBdnhk-5YPQ9Gr3d82NSVLtaMtWteuj-FdITCMVg7Mrux_zmlOqbLu-gi3kqbecZG04sv5x1kxRysVdCi9pMHPP48piJ3Vd4hY2eRr0GlFkskCzcXuhl7d52h1NlqYx8u5rCTr9X3HkGDICmv6a979DvQ",
  },
  // ----------------------------------- createSubcategory ------------------------------------------------ //
  /**
   * descripción: Crea una Subcategoría.
   * tipo: POST
   * url: /subcategory/:categoryId
   * resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   */
  // ----------------------------------- deleteSubcategory ------------------------------------------------ //
  /**
   * descripción: Borrar subcategoría.
   * tipo: DELETE
   * url: /subcategory/:subcategory
   * resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   */
  // ----------------------------------- insertXpikToLibrary ------------------------------------------------ //
  /**
   * descripción: Insertar Xpik a una librería.
   * tipo: POST
   * url: /sub/:subcategoryId/x/:xpikId
   * resultado: resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:
   *   "titles" : {
   *     "en" : "inglés",
   *      "es": "español",
   *       "fr": "francés",
   *       "it": "italiano",
   *       "pt": "portugués",
   *       "ru": "ruso",
   *       "zh": "chino simplificado",
   *       "zh_TW": "chino tradicional"
  }
   */
  // ----------------------------------- updateCategory ------------------------------------------------ //
  /**
   * descripción: Actualizar una Categoría.
   * tipo: PUT
   * url: /category/:categoryId
   * resultado: resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:
   *      "languages" : {
   *            "es": "casa"
   *        }
   */
  // ----------------------------------- updateSubcategory ------------------------------------------------ //
  /**
   * descripción: Actualizar una subcategoría
   * tipo: PUT
   * url: /subcategory/:subcategoryId
   * resultado: resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:
   *      "titles" : {
   *            "es": "casa de campo"
   *      }
   */
  // ----------------------------------- updateXpikLibraryMetadata ------------------------------------------------ //
  /** TODO: Por Revisar por su operatividad actual.
   * descripción: Actualizar campos de Libreria en los distintos idiomas.
   * tipo: PUT
   * url:  /xpiktitles/:xpikId
   * resultado: resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:
   *        "titles" : {
   *            "en" : "vip"
   *         }
   */

  // ----------------------------------- changeXpikLibraryFromSubcategory ------------------------------------------------ //
  /**
   * descripción: Actualizar titulos de la libreria en los distintos idiomas.
   * tipo: PUT
   * url:  /xpiktitles/:xpikId/sub/:subcategoryId
   * resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:   "titles" : {
                "en" : "Hello",
                "es": "Hola",
                "fr": "hola",
                "it": "hola",
                "pt": "hola",
                "ru": "ола",
                "zh": "欢呼声",
                "zh_TW": "你好"
                }
   */

  // ----------------------------------- approveXpikLibrary ------------------------------------------------ //
  /**
   * descripción: Actualiza el estatus a aprobado para el xpik.
   * tipo: PUT
   * url:  /xpiktitles/:xpikId/approve/:approve
   * resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:   {
                "files" : [
                    "archivo5",
                    "archivo4",
                    "asdasdad"]
            }
   */

  // ----------------------------------- deleteXpikMetadata ------------------------------------------------ //
  /**
   * descripción: Borra la metadata del Xpik.
   * tipo: DELETE
   * url:
   * resultado: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body:
   */

  // ----------------------------------- getUserDetails ------------------------------------------------ //
  /**
   * descripción: Obtiene los datos detalle de un determinado usuario.
   * tipo: GET
   * ruta: /user/:handle
   */
  getUserDetails: {
    createdAt: "2021-07-29T18:42:53.516Z",
    email: "useremail@gmail.com",
    userId: "HR4DYg9P4wdpS1p83gp6wEzB4Cg1",
    imageUrl:
      "https://firebasestorage.googleapis.com/v0/b/xpikandroid.appspot.com/o/no-img.png?alt=media",
    role: 2,
    website: "http://personalweb.com",
    location: "Venezuela",
    bio: "asdasdjasdkl",
    handle: "username",
  },
  // ----------------------------------- addTokenDevice ------------------------------------------------ //
  /**
   * descripción: Agrega Token al dispositivo del usuario.
   * tipo: GET
   * ruta: /user/:FCMtoken/token
   * TODO: Pendiente por definir si se usará o descartará.
   */
  // ----------------------------------- setFavoriteXpik ------------------------------------------------ //
  /**
   * descripción: Asigna a un Xpik como favorito.
   * tipo: POST
   * ruta: /user/xpik
   * TODO: Pendiente por definir funciones en el Validator.
   */
  // ----------------------------------- uploadImage ------------------------------------------------ //
  /**
   * descripción: Sube nueva foto a la carpeta de resources/profile de un usuario.
   * tipo: POST
   * ruta: /user/image
   * respuesta: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body: Se pasa archivo indicando la ruta/ubicación de la imagenen en formato jpeg en la computadora local.
   */
  // ----------------------------------- addUserDetail ------------------------------------------------ //
  /**
   * descripción: Actualiza los datos de perfil del usuario.
   * tipo: POST
   * ruta: /user
   * respuesta: Mensaje Successfully en caso de resultado exitoso, en caso contrario mensaje con descripción del error.
   * body: (todos los campos son opcionales, basta que el valor solo esté entre comillas para que no sea tomado en cuenta en la actualización)
   *     {
            "bio":"",
            "website":"https://www.MrRobot.com",
            "location":"Silicon Valley",
            "FCMtoken":""
        }
   */
  // ----------------------------------- getAuthenticatedUser ------------------------------------------------ //
  /**
   * descripción: Obtiene las credenciales del usuario loggeado.
   * tipo: GET
   * ruta: /user
   */
  getAuthenticatedUser: {
    credentials: {
      website: "https://www.MrRobot.com",
      location: "Silicon Valley",
      createdAt: "2021-06-01T20:26:48.647Z",
      handle: "otroantonio",
      role: 2,
      imageUrl:
        "https://firebasestorage.googleapis.com/v0/b/xpikandroid.appspot.com/resources/profile/otroantonio/o/279014066.jpg?alt=media",
      bio: "Hola es un placer, soy nuevo por aqui",
      userId: "sBLaW2InttOb7aBMHxG6KCLV0Lr2",
      email: "antonio@gmail.com",
    },
  },
};

export default API;
