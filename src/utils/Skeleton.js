import React, { Component } from 'react'
import { withStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import placeholder from '../img/placeholder.jpg'

const styles = (theme) => ({
    root: {
        marginBottom: 20
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
        borderRadius:10
    },
});

export class Skeleton extends Component {

    render() {
        const { classes } = this.props;
        return (
            <Grid item xs={12} className={classes.root}>
                {[0, 1, 2].map((value) => (
                    <Grid container justifyContent="center" key={value} >
                        <Grid item xs={3}>
                            <Card>
                                <CardHeader
                                    subheader={"Xpik #" + (value + 1)}
                                />
                                <CardMedia
                                    className={classes.media}
                                    image={placeholder}
                                    title="Paella dish"
                                />
                            </Card>
                        </Grid>
                    </Grid>
                ))}
            </Grid>
        )
    }
}

export default withStyles(styles)(Skeleton)
