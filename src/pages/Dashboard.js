import { Component, Fragment } from 'react';
import LateralMenu from '../components/layout/LateralMenu'
import Skeleton from '../utils/Skeleton'
import Directory from '../utils/Directory'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        marginLeft: 220
    }
});

class Dashboard extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                <div className={classes.root}>
                    <Directory
                        category="Category1"
                        subcategory="Subcategory1"
                        xpiktitle={this.props.title} />
                </div>
                <h1>Dashboard</h1>
                <LateralMenu />
                <Skeleton />
            </Fragment>
        )
    }
}


export default withStyles(styles)(Dashboard);