import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Profile from '../components/Profile';
import Grid from '@material-ui/core/Grid';
import ProfileSkeleton from '../utils/ProfileSkeleton';
import { connect } from 'react-redux';

export class User extends PureComponent {

  constructor(props) {
    super(props);
  }

 
  render() {
    const { loading } = this.props.user;

    let profileMarkup = !loading ? <Profile /> : <ProfileSkeleton />;
    return (
      <Grid container spacing={3} justify="center">
        <Grid item sm={4} xs={12}>
          {profileMarkup}
        </Grid>
      </Grid>
    );
  }
}

User.propType = {
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(User);
