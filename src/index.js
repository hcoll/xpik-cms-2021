import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals'
import './index.css';
import App from './App';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
//Material-UI
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/deepOrange';
import gray from '@material-ui/core/colors/blueGrey';
//Redux Stuff
import { Provider } from 'react-redux';
import store from './redux/store';
import { SET_AUTHENTICATED } from './redux/types';
import { logoutUser, getUserData } from './redux/actions/userActions';

axios.defaults.baseURL =
  'https://us-central1-xpikandroid.cloudfunctions.net/api';


  const token = localStorage.FBIdToken;
  if (token) {
    const decodedToken = jwtDecode(token);
    if (decodedToken.exp * 1000 < Date.now()) {
      store.dispatch(logoutUser());
       //window.location.href = 'login';
    } else {
      store.dispatch({ type: SET_AUTHENTICATED });
      axios.defaults.headers.common['Authorization'] = token;
      store.dispatch(getUserData());
      //window.location.href = 'dashboard';
    }
  }


const theme = createTheme({
  palette: {
    primary: {
      light: orange[200],
      main: orange[500],
      dark: orange[900],
      contrastText: '#fff',
    },
    secondary: {
      light: gray[200],
      main: gray[300],
      dark: gray[800],
      contrastText: '#000',
    },
  },
});

ReactDOM.render(

    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <App />
      </Provider>
    </ThemeProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
